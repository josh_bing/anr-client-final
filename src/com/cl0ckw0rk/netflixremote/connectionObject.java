package com.cl0ckw0rk.netflixremote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class connectionObject {
	 private static connectionObject mInstance = null;
	 private Socket mClient;
	
	 private PrintWriter mMsgSender = null;
	 private InputStream mReader = null;
	 protected connectionObject(){}
	 private boolean clientInstantiated = false;
	 private boolean readerInstantiated = false;
	 private boolean senderInstantiated = false;
	 
	 public static synchronized connectionObject getInstance() {
	      if(mInstance == null) {
	         mInstance = new connectionObject();
	      }
	      return mInstance;
	   }
	 
	 public String connectToServer(String ipAddress)
	 {
			try {
				
				//mClient.setSoTimeout(2000);
				mClient = new Socket();
				mClient.connect(new InetSocketAddress(ipAddress, 6969), 2000);
				clientInstantiated = true;
				mClient.setKeepAlive(true);
				mClient.setTcpNoDelay(true);
				mMsgSender = new PrintWriter(mClient.getOutputStream(),true);
				senderInstantiated = true;
				mClient.setSoTimeout(1500);
				return "success";
			} catch (UnknownHostException e) {
				return "error";
			} catch (IOException e) {
				return "e";
			}
	 }
	 
	 public String dispose()
	 {
		 try {
			Log.w("Blah", "IN DISPOSE");
			
			if(clientInstantiated)
			{
				if(mClient.isConnected() == true)
				{
					Log.w("Blah", "IN CLIENT DISPOSAL");
					mClient.close();
				
				}		
			}
			
			if(readerInstantiated)
			{
				Log.w("Blah", "IN READER DISPOSE");
				mReader.close();
			}
			
			if(senderInstantiated)
			{
				Log.w("Blah", "IN SENDER DISPOSE");
				mMsgSender.flush();
				mMsgSender.close();
			}
			
			clientInstantiated = false;
			readerInstantiated = false;
			senderInstantiated = false;
			
			return "success";
		} catch (IOException e) {
			return "error";
		}
		 
	 }
		 
	 public String sendData(String data)
	 {
		 try
		 {
				mMsgSender.write(data);
				mMsgSender.flush();
				String retVal = handShake();
			 	
			 	return retVal;
		 }
		 catch(Exception e)
		 {
			 return "error";
		 }
		 
	 }
	 
	 public String handShake()
	 {
		 try {
			
			mReader = mClient.getInputStream();
			Log.w("blah", "Got Stream");
			byte[] response = new byte[(int)"ok".length()];
			Log.w("blah", "Creating Byte Array");
			//mClient.setSoTimeout(1500);
			mReader.read(response);
			Log.w("blah", "Reading Response");
			
			String rspnse = "";
			rspnse = new String(response);
			Log.w("blah", "Made String");
			Log.w("blah", "RESPONSE: " + rspnse);
			
			if(!rspnse.equals(""))
			{
				return "success";
			}
			
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "error";
		}
		 
		 return "error";
	 }
	 
}
