package com.cl0ckw0rk.netflixremote;

import android.R.color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MouseKey extends Activity {

	View mouseView;
	float pastX = 0;
	int xCoord = 0;
	int yCoord = 0;
	int commandLimit = 0;
	float pastY = 0;
	Button mouseClick;
	Button keyboardInvoke;
	String command;
	Button mouseUp, mouseDown, mouseLeft, mouseRight, mouseNe, mouseSw, mouseNw, mouseSe;
	Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b0;
	Button ba,bb,bc,bd,be,bf,bg,bh,bi,bj,bk,bl,bm,bn,bo,bp,bq,br,bs,bt,bu,bv,bw,bx,by,bz;
	Button space,enter,back;
	EditText invis;
	private Handler handler = new Handler();
	
	
	private Runnable buttonHeld = new Runnable() {
	    @Override
	    public void run() {
	    	new sendDataTask().execute(command);
	        handler.postDelayed(this, 100);
	    }
	};
	
	
	OnTouchListener mouseListener = new OnTouchListener()
	{

		@Override
		public boolean onTouch(View arg0, MotionEvent arg1) {
			switch(arg1.getAction())
			{
			/*case MotionEvent.ACTION_MOVE:	
                xCoord = (int)(arg1.getX() - pastX);
                yCoord = (int)(arg1.getY() - pastY);
                String toSend = "MC[" + xCoord + " " + yCoord + "]";
                new sendDataTask().execute(toSend);
                return true;*/
			case MotionEvent.ACTION_DOWN:
				/*Log.w("blah", "Getting X and Y");
				pastX = arg1.getX();
				pastY = arg1.getY();*/
				if(arg0.equals(mouseUp))
				{
					command = "MC[0 -7]";
				}
				else
				if(arg0.equals(mouseDown))
				{
					command = "MC[0 7]";
				}
				else
				if(arg0.equals(mouseRight))
				{
					command = "MC[7 0]";
				}
				else
				if(arg0.equals(mouseLeft))
				{
					command = "MC[-7 0]";
				}
				else
				if(arg0.equals(mouseNe))
				{
					command = "MC[7 -7]";
				}
				else
				if(arg0.equals(mouseSe))
				{
					command = "MC[7 7]";
				}
				else
				if(arg0.equals(mouseSw))
				{
					command = "MC[-7 7]";
				}
				else
				if(arg0.equals(mouseNw))
				{
					command = "MC[-7 -7]";
				}
					
				handler.post(buttonHeld);
				return true;
				
			case MotionEvent.ACTION_UP:
				handler.removeCallbacks(buttonHeld);
				return true;
			}

			
			return true;
		}
		
	};
	
	OnClickListener keyListener = new OnClickListener()
	{

		@Override
		public void onClick(View v) {
			switch(v.getId())
			{
				case R.id.clickMouse:
					new sendDataTask().execute("MOUSECLICK");
					break;
				case R.id.button0:
					new sendDataTask().execute("*[0]");
					break;
				case R.id.button1:
					new sendDataTask().execute("*[1]");
					break;
				case R.id.button2:
					new sendDataTask().execute("*[2]");
					break;
				case R.id.button3:
					new sendDataTask().execute("*[3]");
					break;
				case R.id.button4:
					new sendDataTask().execute("*[4]");
					break;
				case R.id.button5:
					new sendDataTask().execute("*[5]");
					break;
				case R.id.button6:
					new sendDataTask().execute("*[6]");
					break;
				case R.id.button7:
					new sendDataTask().execute("*[7]");
					break;
				case R.id.button8:
					new sendDataTask().execute("*[8]");
					break;
				case R.id.button9:
					new sendDataTask().execute("*[9]");
					break;
				case R.id.buttonQ:
					new sendDataTask().execute("*[q]");
					break;
				case R.id.buttonW:
					new sendDataTask().execute("*[w]");
					break;
				case R.id.buttonE:
					new sendDataTask().execute("*[e]");
					break;
				case R.id.buttonR:
					new sendDataTask().execute("*[r]");
					break;
				case R.id.buttonT:
					new sendDataTask().execute("*[t]");
					break;
				case R.id.buttonY:
					new sendDataTask().execute("*[y]");
					break;
				case R.id.buttonU:
					new sendDataTask().execute("*[u]");
					break;
				case R.id.buttonI:
					new sendDataTask().execute("*[i]");
					break;
				case R.id.buttonO:
					new sendDataTask().execute("*[o]");
					break;
				case R.id.buttonP:
					new sendDataTask().execute("*[p]");
					break;
				case R.id.buttonA:
					new sendDataTask().execute("*[a]");
					break;
				case R.id.buttonS:
					new sendDataTask().execute("*[s]");
					break;
				case R.id.buttonD:
					new sendDataTask().execute("*[d]");
					break;
				case R.id.buttonF:
					new sendDataTask().execute("*[f]");
					break;
				case R.id.buttonG:
					new sendDataTask().execute("*[g]");
					break;
				case R.id.buttonH:
					new sendDataTask().execute("*[h]");
					break;
				case R.id.buttonJ:
					new sendDataTask().execute("*[j]");
					break;
				case R.id.buttonK:
					new sendDataTask().execute("*[k]");
					break;
				case R.id.buttonL:
					new sendDataTask().execute("*[l]");
					break;
				case R.id.buttonZ:
					new sendDataTask().execute("*[z]");
					break;
				case R.id.buttonX:
					new sendDataTask().execute("*[x]");
					break;
				case R.id.buttonC:
					new sendDataTask().execute("*[c]");
					break;
				case R.id.buttonV:
					new sendDataTask().execute("*[v]");
					break;
				case R.id.buttonB:
					new sendDataTask().execute("*[b]");
					break;
				case R.id.buttonN:
					new sendDataTask().execute("*[n]");
					break;
				case R.id.buttonM:
					new sendDataTask().execute("*[m]");
					break;
				case R.id.buttonSpace:
					new sendDataTask().execute("*[ ]");
					break;
				case R.id.buttonEnter:
					new sendDataTask().execute("*[{ENTER}]");
					break;
				case R.id.buttonBack:
					new sendDataTask().execute("*[{BACKSPACE}]");
					break;
			}
			
			//Log.w("Blah", "CLICKING: " + xCoord + ", "+ yCoord);	
		}
		
	};
	
	OnClickListener clickKeyboardListener = new OnClickListener()
	{

		@Override
		public void onClick(View v) {
			invis.setVisibility(View.VISIBLE);
			invis.setFocusable(true);
			invis.setFocusableInTouchMode(true);
			invis.requestFocus();
			InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			inputMethodManager.toggleSoftInputFromWindow(invis.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
			
			//invis.setOnKeyListener(keyListener);
			//InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        	//imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		}
		
	};
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mouse_key);
		//mouseView = findViewById(R.id.Mouse);
		//mouseView.setOnTouchListener(mouseListener);
		mouseClick = (Button)findViewById(R.id.clickMouse);
		
		mouseUp = (Button)findViewById(R.id.upButton);
		mouseDown = (Button)findViewById(R.id.downButton);
		mouseRight = (Button)findViewById(R.id.rightButton);
		mouseLeft = (Button)findViewById(R.id.leftButton);
		mouseSe = (Button)findViewById(R.id.southEastButton);
		mouseNe = (Button)findViewById(R.id.northEastButton);
		mouseSw = (Button)findViewById(R.id.southWestButton);
		mouseNw = (Button)findViewById(R.id.northWestButton);
		
		b1 = (Button)findViewById(R.id.button1);
		b2 = (Button)findViewById(R.id.button2);
		b3 = (Button)findViewById(R.id.button3);
		b4 = (Button)findViewById(R.id.button4);
		b5 = (Button)findViewById(R.id.button5);
		b6 = (Button)findViewById(R.id.button6);
		b7 = (Button)findViewById(R.id.button7);
		b8 = (Button)findViewById(R.id.button8);
		b9 = (Button)findViewById(R.id.button9);
		b0 = (Button)findViewById(R.id.button0);
		
		ba = (Button)findViewById(R.id.buttonA);
		bb = (Button)findViewById(R.id.buttonB);
		bc = (Button)findViewById(R.id.buttonC);
		bd = (Button)findViewById(R.id.buttonD);
		be = (Button)findViewById(R.id.buttonE);
		bf = (Button)findViewById(R.id.buttonF);
		bg = (Button)findViewById(R.id.buttonG);
		bh = (Button)findViewById(R.id.buttonH);
		bi = (Button)findViewById(R.id.buttonI);
		bj = (Button)findViewById(R.id.buttonJ);
		bk = (Button)findViewById(R.id.buttonK);
		bl = (Button)findViewById(R.id.buttonL);
		bm = (Button)findViewById(R.id.buttonM);
		bn = (Button)findViewById(R.id.buttonN);
		bo = (Button)findViewById(R.id.buttonO);
		bp = (Button)findViewById(R.id.buttonP);
		bq = (Button)findViewById(R.id.buttonQ);
		br = (Button)findViewById(R.id.buttonR);
		bs = (Button)findViewById(R.id.buttonS);
		bt = (Button)findViewById(R.id.buttonT);
		bu = (Button)findViewById(R.id.buttonU);
		bv = (Button)findViewById(R.id.buttonV);
		bw = (Button)findViewById(R.id.buttonW);
		bx = (Button)findViewById(R.id.buttonX);
		by = (Button)findViewById(R.id.buttonY);
		bz = (Button)findViewById(R.id.buttonZ);
		
		space = (Button)findViewById(R.id.buttonSpace);
		back = (Button)findViewById(R.id.buttonBack);
		enter = (Button)findViewById(R.id.buttonEnter);
		
		
		//invis = (EditText) findViewById(R.id.editText1);
		//invis.setBackgroundColor(color.transparent);
		//invis.setVisibility(View.GONE);
		mouseClick.setOnClickListener(keyListener);
		
		b1.setOnClickListener(keyListener);
		b2.setOnClickListener(keyListener);
		b3.setOnClickListener(keyListener);
		b4.setOnClickListener(keyListener);
		b5.setOnClickListener(keyListener);
		b6.setOnClickListener(keyListener);
		b7.setOnClickListener(keyListener);
		b8.setOnClickListener(keyListener);
		b9.setOnClickListener(keyListener);
		b0.setOnClickListener(keyListener);
		
		ba.setOnClickListener(keyListener);
		bb.setOnClickListener(keyListener);
		bc.setOnClickListener(keyListener);
		bd.setOnClickListener(keyListener);
		be.setOnClickListener(keyListener);
		bf.setOnClickListener(keyListener);
		bg.setOnClickListener(keyListener);
		bh.setOnClickListener(keyListener);
		bi.setOnClickListener(keyListener);
		bj.setOnClickListener(keyListener);
		bk.setOnClickListener(keyListener);
		bl.setOnClickListener(keyListener);
		bm.setOnClickListener(keyListener);
		bn.setOnClickListener(keyListener);
		bo.setOnClickListener(keyListener);
		bp.setOnClickListener(keyListener);
		bq.setOnClickListener(keyListener);
		br.setOnClickListener(keyListener);
		bs.setOnClickListener(keyListener);
		bt.setOnClickListener(keyListener);
		bu.setOnClickListener(keyListener);
		bv.setOnClickListener(keyListener);
		bw.setOnClickListener(keyListener);
		bx.setOnClickListener(keyListener);
		by.setOnClickListener(keyListener);
		bz.setOnClickListener(keyListener);
		
		space.setOnClickListener(keyListener);
		enter.setOnClickListener(keyListener);
		back.setOnClickListener(keyListener);
		
		
		
		mouseUp.setOnTouchListener(mouseListener);
		mouseDown.setOnTouchListener(mouseListener);
		mouseLeft.setOnTouchListener(mouseListener);
		mouseRight.setOnTouchListener(mouseListener);
		mouseSe.setOnTouchListener(mouseListener);
		mouseSw.setOnTouchListener(mouseListener);
		mouseNw.setOnTouchListener(mouseListener);
		mouseNe.setOnTouchListener(mouseListener);
		//invis.setFocusable( false );
		//invis.setFocusableInTouchMode( false );
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_mouse_key, menu);
		return true;
	}
	
	private class sendDataTask extends AsyncTask<String, Void, String>
	{

		@Override
		protected String doInBackground(String... params) {
			String result = connectionObject.getInstance().sendData(params[0]);
			if(result.equals("error"))
			{
				connectionObject.getInstance().dispose();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result)
		{
			if(!result.equals("success"))
			  {
				Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_SHORT).show();
				//status.setText("Connected: NO");
			  }
			else
			  {
				//status.setText("Connected: YES");
			  }
			  
		}
		
	}
	
	
	
	

}
