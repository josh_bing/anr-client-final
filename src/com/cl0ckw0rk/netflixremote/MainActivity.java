package com.cl0ckw0rk.netflixremote;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	
	SharedPreferences prefs;
	//TextView status;
	ImageView playPause;
	ImageView dispInfo;
	ImageView volUp;
	ImageView volDown;
	ImageView forward;
	ImageView backward;
	

	
	OnClickListener playPauseListener = new OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			 new sendDataTask().execute("PAUSE");
		}
		
	};
	
	
	OnClickListener dispInfoListener = new OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			new sendDataTask().execute("DISPINFO");
		}
		
	};
	
	OnClickListener volDownListener = new OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			new sendDataTask().execute("VOLDOWN");
		}
		
	};
	
	OnClickListener volUpListener = new OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			new sendDataTask().execute("VOLUP");
		}
		
	};
	
	OnClickListener forwardListener = new OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			new sendDataTask().execute("FORWARD");
		}
		
	};
	
	OnClickListener backwardListener = new OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			new sendDataTask().execute("BACKWARD");
		}
		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		
	    playPause = (ImageView) findViewById(R.id.playPause);
		playPause.setOnClickListener(playPauseListener);
		dispInfo = (ImageView) findViewById(R.id.info);
		dispInfo.setOnClickListener(dispInfoListener);
		volUp = (ImageView) findViewById(R.id.volUp);
		volUp.setOnClickListener(volUpListener);
		volDown = (ImageView) findViewById(R.id.volDown);
		volDown.setOnClickListener(volDownListener);
		forward = (ImageView) findViewById(R.id.fastforward);
		forward.setOnClickListener(forwardListener);
		backward = (ImageView) findViewById(R.id.rewind);
		backward.setOnClickListener(backwardListener);
		
		
		
		prefs = getSharedPreferences("prefs", MODE_PRIVATE);
		//status = (TextView) findViewById(R.id.connStatus);

			
		enterIp();
		
		
	
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	
	public void enterIp()
	{
		
		AlertDialog.Builder IPAlert = new AlertDialog.Builder(this);
		IPAlert.setMessage("Enter the Server's IP Address");
		
		final EditText input = new EditText(this);
		input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		input.setText(prefs.getString("LastIpAddress", "0.0.0.0"));
		IPAlert.setView(input);
		
		IPAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			  String addr = input.getText().toString();
			  new connectToServerTask().execute(addr);
			  dialog.dismiss();
			  
			}});
		IPAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) {
			    dialog.dismiss();
			  }
			});
		IPAlert.show();
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.menu_connect:
			connectionObject.getInstance().dispose();
			//status.setText("Connected: NO");
			enterIp();
			return true;
		case R.id.menu_mouseKey:
			Intent mouseIntent = new Intent(MainActivity.this, MouseKey.class);
			MainActivity.this.startActivity(mouseIntent);
		}
		return false;
	}
	
	private class connectToServerTask extends AsyncTask<String, Void, String>
	{

		@Override
		protected String doInBackground(String... arg0) {
			String result = connectionObject.getInstance().connectToServer(arg0[0]);
			connectionObject.getInstance().sendData("a");
			if(!result.equals("success"))
			{
				//connectionObject.getInstance().dispose();
			}
			SharedPreferences.Editor e = prefs.edit();
			e.putString("LastIpAddress", arg0[0]); // add or overwrite someValue
			e.commit(); 
			
			return result;
		}
		
		@Override
		protected void onPostExecute(String result)
		{
			if(!result.equals("success"))
			  {
				Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_SHORT).show();
				//status.setText("Connected: NO");
			  }
			else
			  {
				//status.setText("Connected: YES");
			  }
			  
		}
		
	}
	
	private class sendDataTask extends AsyncTask<String, Void, String>
	{

		@Override
		protected String doInBackground(String... params) {
			String result = connectionObject.getInstance().sendData(params[0]);
			if(result.equals("error"))
			{
				connectionObject.getInstance().dispose();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result)
		{
			if(!result.equals("success"))
			  {
				Toast.makeText(getApplicationContext(), "Connection Error", Toast.LENGTH_SHORT).show();
				//status.setText("Connected: NO");
			  }
			else
			  {
				//status.setText("Connected: YES");
			  }
			  
		}
		
	}
	
	
	

}
